package org.randomcraft.RCEventsBareBones.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEventsBareBones.RCEventsBareBones;

public class EventEnd implements CommandExecutor, Listener{
	
	private RCEventsBareBones plugin = RCEventsBareBones.getPlugin(RCEventsBareBones.class);
	
	ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = (prefix + "You do not have permission!");
	public String args = (prefix + "Invalid Arguments!");	
	
	public String event = plugin.getConfig().getString("event.startendcommand");
	
	public ArrayList<Player> ic = RCEventsBareBones.getIC();
	public ArrayList<Player> solved = RCEventsBareBones.getSolved();
	public ArrayList<Player> winner = RCEventsBareBones.getWinner();
	public ArrayList<Player> spec = RCEventsBareBones.getSpec();
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender.hasPermission("randomcraft.eventend")) {
			if(cmd.getName().equalsIgnoreCase("eventend")) {
				
				if(args.length == 2) {
					if(args[0].equals(event)) {
							
						ConfigurationSection ev = plugin.getConfig().getConfigurationSection("event");
						ConfigurationSection maplo = plugin.getConfig().getConfigurationSection("event.maplocation");

						
						String hub = plugin.getConfig().getString("event.hublocation.world");
						String endmsg = ev.getString("endmsg");
						String ended = ev.getString("endedmsg");
						String leavemsg = ev.getString("leavemsg");
						String world = maplo.getString("world");

						Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + endmsg));
						plugin.getConfig().set("event.started", 2);
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {

							@Override
							public void run() {
								
								
								Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + ended));
								
								for(Player all : Bukkit.getOnlinePlayers()) {
									if(all.getWorld().getName().equals(world)) {
										
										ic.remove(all);
										all.setGameMode(GameMode.SURVIVAL);
																
										
										World worldvalue = Bukkit.getServer().getWorld(hub);
										float yaw = plugin.getConfig().getInt("event.maplocation.yaw");
										float pitch = plugin.getConfig().getInt("event.maplocation.pitch");
										double x = plugin.getConfig().getDouble ("event.maplocation.x");
										double y = plugin.getConfig().getDouble ("event.maplocation.y");					
										double z = plugin.getConfig().getDouble ("event.maplocation.z");
										
										Location hub = new Location(worldvalue, x ,y, z, yaw, pitch);
										all.teleport(hub);			
										all.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + leavemsg));
										
										return;
									}
									
									return;
								}
								
							}
							
							
							
						}, 1200);
						return true;
					}else {
						sender.sendMessage(args);
						return true;
					}
				}else {
					sender.sendMessage(args);
					return true;
				}
			}
			return true;
		}else {
			sender.sendMessage(noperms);
			return true;
		}
	}

}
