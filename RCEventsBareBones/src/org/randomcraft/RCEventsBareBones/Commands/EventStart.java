package org.randomcraft.RCEventsBareBones.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEventsBareBones.RCEventsBareBones;

public class EventStart implements CommandExecutor, Listener{

	private RCEventsBareBones plugin = RCEventsBareBones.getPlugin(RCEventsBareBones.class);
	
	ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = (prefix + "You do not have permission!");
	public String args = (prefix + "Invalid Arguments!");	
	
	public String event = plugin.getConfig().getString("event.startendcommand");
	
	public ArrayList<Player> ic = RCEventsBareBones.getIC();
	public ArrayList<Player> solved = RCEventsBareBones.getSolved();
	public ArrayList<Player> winner = RCEventsBareBones.getWinner();
	public ArrayList<Player> spec = RCEventsBareBones.getSpec();
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(sender.hasPermission("randomcraft.event.start")) {
			if(cmd.getName().equalsIgnoreCase("eventstart")) {
				
				if(args.length == 2) {
					if(args[0].equals(event)){
						
						ConfigurationSection ev = plugin.getConfig().getConfigurationSection("event");
						ConfigurationSection lb = plugin.getConfig().getConfigurationSection("event.eventlobby");
						ConfigurationSection maplo = plugin.getConfig().getConfigurationSection("event.maplocation");

						
						String lobby = lb.getString("world");
						String startmsg = ev.getString("startmsg");
						String started = ev.getString("startedmsg");
						String world = maplo.getString("world");
						String joinmsg = ev.getString("joinmsg");
						
						Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + startmsg));
						plugin.getConfig().set("event.started", 1);
						plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
							
							@Override
							public void run() {
								
								Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', prefix + started));
								
								for(Player all : Bukkit.getOnlinePlayers()) {
									
									if(all.getWorld().getName().equals(lobby)) {
										
										ic.add(all);
										all.setGameMode(GameMode.SPECTATOR);
										
										
										World worldvalue = Bukkit.getServer().getWorld(world);
										float yaw = plugin.getConfig().getInt("event.maplocation.yaw");
										float pitch = plugin.getConfig().getInt("event.maplocation.pitch");
										double x = plugin.getConfig().getDouble ("event.maplocation.x");
										double y = plugin.getConfig().getDouble ("event.maplocation.y");					
										double z = plugin.getConfig().getDouble ("event.maplocation.z");
										
										Location map = new Location(worldvalue, x ,y, z, yaw, pitch);
										all.teleport(map);			
										all.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + joinmsg));
										
										System.out.println(x);
										System.out.println(y);
										System.out.println(z);

										return;
										
									}
									
									return;
								}
								
							}
							
						}, 1200);
						
						return true;
					}else {
						sender.sendMessage(args);
						return true;
					}
				}else {
					sender.sendMessage(args);
					return true;
				}
				
			}
			return true;
		}else {
			sender.sendMessage(noperms);
			return true;
		}
	}

}
