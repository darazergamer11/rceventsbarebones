package org.randomcraft.RCEventsBareBones.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEventsBareBones.RCEventsBareBones;

public class EventJoin implements CommandExecutor, Listener{

	private RCEventsBareBones plugin = RCEventsBareBones.getPlugin(RCEventsBareBones.class);
	
	ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = (prefix + "You do not have permission!");
	public String args = (prefix + "Invalid Arguments!");	
	
	public String event = plugin.getConfig().getString("event.startendcommand");
	
	public ArrayList<Player> ic = RCEventsBareBones.getIC();
	public ArrayList<Player> solved = RCEventsBareBones.getSolved();
	public ArrayList<Player> winner = RCEventsBareBones.getWinner();
	public ArrayList<Player> spec = RCEventsBareBones.getSpec();
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equals("eventjoin")) {
			if(sender instanceof Player) {
				
				Player player = (Player) sender;
				
				if(player.hasPermission("randomcraft.eventjoin")) {
					
					if(args.length == 1) {
						if(args[0].equals(event)) {
							if(!(plugin.getConfig().getInt("event.started") == 1)) {
								
								ic.add(player);
							
							}else {
								sender.sendMessage(prefix + "This event hasn't started!");
								return true;
								}
							
						}else {
							sender.sendMessage(args);
							return true;
						}
					}else {
						sender.sendMessage(args);
						return true;
					}
				}else {
					sender.sendMessage(noperms);
					return true;
				}
			}else {
				sender.sendMessage(prefix + "You must be a player to execute this command!");
				return true;
			}
		}else {
			sender.sendMessage(args);
			return true;
		}

		
		return true;
	}

}
