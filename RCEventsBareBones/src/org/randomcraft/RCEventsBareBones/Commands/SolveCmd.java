package org.randomcraft.RCEventsBareBones.Commands;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.randomcraft.RCEventsBareBones.RCEventsBareBones;

public class SolveCmd implements CommandExecutor, Listener{

	private RCEventsBareBones plugin = RCEventsBareBones.getPlugin(RCEventsBareBones.class);
	
	ConsoleCommandSender console = Bukkit.getServer().getConsoleSender();
	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = ("You do not have permission!");
	public String args = ("Invalid Arguments!");	
	
	public String solve = plugin.getConfig().getString("event.solvedcode");
	
	public ArrayList<Player> ic = RCEventsBareBones.getIC();
	public ArrayList<Player> solved = RCEventsBareBones.getSolved();
	public ArrayList<Player> winner = RCEventsBareBones.getWinner();
	public ArrayList<Player> spec = RCEventsBareBones.getSpec();
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("solve")) {
			
			if(args.length == 1) {
				if(args[0].equals(solve)) {
					if(!(plugin.getConfig().getInt("event.started") == 1)) {
						
					
					if(sender instanceof Player) {
						Player player = (Player) sender;
						
						if(ic.contains(player)) {
						
						solved.add((Player) sender);
						spec.add((Player) sender);
						
						((Player) sender).setGameMode(GameMode.SPECTATOR);
						
						
						if(winner.isEmpty()) {
							winner.add((Player) sender);
							sender.sendMessage(prefix + "You have succesfully solved the challenge, and won!");
							console.sendMessage(sender + "has won the challenge!");
							return true;
							
						}else {
							sender.sendMessage(prefix + "You have succesfully solved the challenge, however " + winner.listIterator() + "has already won!");
							console.sendMessage(sender + "has completed the challenge!");
							return true;
						}
					}else {
						sender.sendMessage(prefix + "You must be in the challenge to execute this command!");
					}
					}else {
						sender.sendMessage(prefix + "You must be a player to execute this command!");
						return true;
				}
					}else {
						sender.sendMessage(prefix + "This event hasn't started!");
						return true;
			}
		}else {
			sender.sendMessage(prefix + "Incorrect! Try again");
			console.sendMessage(sender + "has failed the challenge");
			return true;
		}
	}
		
		
		return true;
}
		return false;

	}
}
