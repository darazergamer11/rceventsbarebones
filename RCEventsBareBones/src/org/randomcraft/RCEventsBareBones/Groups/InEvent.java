package org.randomcraft.RCEventsBareBones.Groups;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.randomcraft.RCEventsBareBones.RCEventsBareBones;

public class InEvent implements Listener{

	
	public String prefix = (ChatColor.GRAY + "(" + ChatColor.DARK_AQUA + "RC" + ChatColor.GRAY + ") ");
	public String noperms = (prefix + "You do not have permission!");
	
	
	public ArrayList<Player> ic = RCEventsBareBones.getIC();
	public ArrayList<Player> solved = RCEventsBareBones.getSolved();
	public ArrayList<Player> winner = RCEventsBareBones.getWinner();
	public ArrayList<Player> spec = RCEventsBareBones.getSpec();

	
	@EventHandler
	public void onBreak(BlockBreakEvent event) {
		
		Player player = event.getPlayer();
		
		if(ic.contains(player)) {
			event.setCancelled(true);
			player.sendMessage(noperms);
			return;
		}else {
			event.setCancelled(false);
			return;
		}
		
		
	}
	
	@EventHandler
	public void onBuild(BlockPlaceEvent event) {
		
		Player player = event.getPlayer();
		
		if(ic.contains(player)) {
			event.setCancelled(true);
			player.sendMessage(noperms);
			return;
		}else {
			event.setCancelled(false);
			return;
		}
		
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		
		Player player = event.getPlayer();
		
		if(ic.contains(player)) {
			event.setCancelled(true);
			
			for(Player ic : ic) {
				ic.sendMessage(prefix + "[Challenge] " + player.getDisplayName() + ChatColor.GRAY + " � " + ChatColor.RESET + event.getMessage());
				return;
			}
			
		}else {
			event.setCancelled(false);
			return;
		}
	}
}
